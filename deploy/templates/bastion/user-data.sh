#!/bin/bash

# Update package
sudo yum update -y
# Install and Start Docker
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service  # Auto activation
sudo systemctl start docker.service
# Add amazon-linux2 defualt user to docker group
sudo usermod -aG docker ec2-user