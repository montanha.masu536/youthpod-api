# Default sample values to share variables among team members
# for information that they are needed for terraform to run
db_username         = "youthpod"
db_password         = "changeme"
django_secret_key   = "changeme"
email_host_password = "changeme"