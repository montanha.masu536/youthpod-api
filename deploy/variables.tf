variable "prefix" {
  type        = string
  default     = "ypa"
  description = "Prefix that will be used for naming resources. Stands for youthpod-api"
}

variable "project" {
  default = "youthpod-api"
}

variable "contact" {
  default = "montanha.masu536@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "youthpod-api-bastion"
}

variable "ecr_image_app" {
  description = "ECR image for app container"
  default     = "421244907237.dkr.ecr.ap-northeast-1.amazonaws.com/youthpod-api:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy container"
  default     = "421244907237.dkr.ecr.ap-northeast-1.amazonaws.com/youthpod-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "email_host_password" {
  description = "Email host password for gmail stmp"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "youthpod.org"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
