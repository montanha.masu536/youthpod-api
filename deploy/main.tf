terraform {
  backend "s3" {
    bucket         = "youthpod-api-tfstate"
    key            = "youthpod-api.tfstate"
    region         = "ap-northeast-1"
    encrypt        = true
    dynamodb_table = "youthpod-api-tfstate-lock"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.25.0"
    }
  }
}

provider "aws" {
  region = "ap-northeast-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    MannagedBy  = "Terraform"
  }
}

# Retrieve current region to avoid hard coding
data "aws_region" "current" {}
