from datetime import datetime
from django.conf import settings
from django.contrib.auth import get_user_model
from user_api.models import Profile
from band_api.models import Band, BandMember, LiveInfo, CommentOnLiveInfo
from music_api.models import Music, Playlist, Like, CommentOnMusic
# Email & csv
from django.core.mail import EmailMessage
import csv
from io import StringIO


def data_report_send_email(send_to: list = ['montanha.masu536@gmail.com', 'tezuka.y@icloud.com']):
    """
    12時間に一回全テーブルのデータのcountを取得してcsvファイルをemailで送信する。(自動化未実装)
    """
    # Initialize
    now = datetime.now().strftime("%m-%d-%Y_%H:%M")
    email = EmailMessage(f'【Data Report】YouthPod 本日のデータ({now})', f'開発お疲れ様です。データ集計の記録です。\n計測時刻は({now})です。', settings.EMAIL_HOST_USER, send_to)
    attachment_csv_file = StringIO()
    writer = csv.writer(attachment_csv_file)
    # write labels to the first row
    labels = ['datetime', 'num_of_leaders', 'user', 'profile', 'band', 'band_member', 'live_info', 'music', 'playlist', 'like', 'comment_on_live_info', 'comment_on_music']
    writer.writerow(labels)
    # Initialize the second row with two information
    num_of_leaders = str(BandMember.objects.filter(is_leader=True).count())
    row = [now, num_of_leaders]
    # Append the rest info and write the second row
    model_array = [get_user_model(), Profile, Band, BandMember, LiveInfo, Music, Playlist, Like, CommentOnLiveInfo, CommentOnMusic]
    for model in model_array:
        row.append(str(model.objects.all().count()))
    writer.writerow(row)
    # Attach the file as a csv file, then send
    email.attach(f'{now}_analytics_data.csv', attachment_csv_file.getvalue(), 'text/csv')
    email.send(fail_silently=False)
