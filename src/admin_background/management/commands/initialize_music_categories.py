from youthpod_api.utils import initialize_music_categories
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    """Initialize Music Categories"""
    def handle(self, *args, **options):
        self.stdout.write('Initializing Music Categories...')
        try:
            flag = initialize_music_categories()
            if flag:
                self.stdout.write(self.style.SUCCESS('Music categories were initialized.'))
            else:
                self.stdout.write(self.style.SUCCESS('Music categories already exist.'))
        except Exception:
            raise CommandError('Command initialize_music_categories FAILED.')
