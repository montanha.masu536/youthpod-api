from django.urls import path
from .views import run_data_report_send_email_when_POST, ReportCreateView, ReportListView, ReportRetrieveDestroyView, UserActivityControlView

urlpatterns = [
    path('data_report_send_email/', run_data_report_send_email_when_POST),
    path('report/list/', ReportListView.as_view()),
    path('report/create/', ReportCreateView.as_view()),
    path('report/<int:pk>/', ReportRetrieveDestroyView.as_view()),
    path('user_control/be_careful_enough/<int:pk>/', UserActivityControlView.as_view()),
]
