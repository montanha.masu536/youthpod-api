from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from user_api.permissions import IsSuperuser
from .tasks import data_report_send_email
from rest_framework import generics
from .models import Report
from .serializers import ReportSerializer, UserActivityControlSerializer
from django.contrib.auth import get_user_model


# Data Report via Email
@api_view(['GET', 'POST'])
@permission_classes([IsSuperuser])
def run_data_report_send_email_when_POST(request):
    """
    データcsvファイルを手動で送る。celeryにするかdjango-background-tasksにするか検討中のため、自動送信はver2で実装する。
    GET: 総Task量と、最初のタスクの内容を取得
    POST: 送信
    """
    if request.method == 'GET':
        res = {'message': '[Beta] superuserのみData Reportを送信できます'}
        return Response(res)
    elif request.method == 'POST':
        data_report_send_email()
        return Response(
            {
                "status": "Email was sent."
            }
        )


# 通報機能(Report)
class ReportCreateView(generics.CreateAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ReportListView(generics.ListAPIView):
    """Report一覧"""
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = [IsSuperuser]


class ReportRetrieveDestroyView(generics.RetrieveDestroyAPIView):
    """Report確認・削除"""
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = [IsSuperuser]


# ユーザー停止・復帰・強制削除機能
class UserActivityControlView(generics.RetrieveUpdateDestroyAPIView):
    """注意して使用すること"""
    queryset = get_user_model().objects.all()
    serializer_class = UserActivityControlSerializer
    permission_classes = [IsSuperuser]
