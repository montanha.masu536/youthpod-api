from django.db import models
from django.contrib.auth import get_user_model


class Report(models.Model):
    """通報用モデル"""
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    object_url = models.URLField(max_length=255)
    description = models.TextField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.object_url
