from rest_framework import serializers
from .models import Report
from django.contrib.auth import get_user_model


class ReportSerializer(serializers.ModelSerializer):
    user_email = serializers.CharField(read_only=True, source='user.email')
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = Report
        fields = ('id', 'user', 'user_email', 'object_url', 'description', 'created_at')
        read_only_fields = ('id', 'user', 'user_email', 'created_at')


class UserActivityControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'is_active')
        read_only_fields = ('id', 'email')
