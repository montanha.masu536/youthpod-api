from django.apps import AppConfig


class AdminBackgroundConfig(AppConfig):
    name = 'admin_background'
