# Generated by Django 3.2.2 on 2021-05-15 07:29

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import music_api.models
import music_api.validators
import uuid
import youthpod_api.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BaseMusicFolder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('num_of_musics', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
        migrations.CreateModel(
            name='CommentOnMusic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.TextField(max_length=1000)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Like',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
        migrations.CreateModel(
            name='Music',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=50)),
                ('music', models.FileField(max_length=255, upload_to=music_api.models.music_upload_to, validators=[music_api.validators.validate_music_file_ext, youthpod_api.validators.validate_music_file])),
                ('img', models.ImageField(max_length=255, upload_to=music_api.models.image_upload_to, validators=[django.core.validators.validate_image_file_extension, youthpod_api.validators.validate_image_size])),
                ('lyrics', models.TextField(blank=True, max_length=2000, null=True)),
                ('length', models.IntegerField(blank=True, null=True)),
                ('played_count', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['updated_at'],
            },
        ),
        migrations.CreateModel(
            name='MusicCategory',
            fields=[
                ('id', models.CharField(max_length=255, primary_key=True, serialize=False, unique=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='LikeCount',
            fields=[
                ('music', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='like_count', serialize=False, to='music_api.music')),
                ('count', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Playlist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=30)),
                ('num_of_musics', models.IntegerField(default=0)),
                ('total_length', models.IntegerField(default=0)),
                ('is_private', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('musics', models.ManyToManyField(to='music_api.Music')),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
    ]
