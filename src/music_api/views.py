# permissions
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from band_api.permissions import IsLeaderOrReadOnly
from .permissions import IsTheMusicsBandsLeaderOrReadOnly
from user_api.permissions import IsOwner, IsOwnerOrReadOnly, IsSuperuser
# models
from .models import Music, BaseMusicFolder, Playlist, Like, LikeCount, CommentOnMusic, MusicCategory
from band_api.models import Band
# serializers
from .serializers import MusicSerializer, MusicPlayedCountSerializer, BaseMusicFolderSerializer, PlaylistSerializer, BandMusicsSerializer, LikeSerializer, MusicLikedUsersSerializer, CommentOnMusicSerializer, MusicCategorySerializer
# views
from rest_framework import generics, status
from rest_framework.decorators import api_view, permission_classes, throttle_classes
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from .throttles import AnonAddPlayedCountRateThrottle, UserAddPlayedCountRateThrottle
# filters
from rest_framework import filters
from django_filters import rest_framework as df_filters
# exceptions
from rest_framework.exceptions import NotFound
# others
from django.db.models import F
from youthpod_api.utils import initialize_music_categories as initialize
from youthpod_api.utils import bool_query_params


class MusicView(generics.ListCreateAPIView):
    """
    Music を作成。
    """
    queryset = Music.objects.all()
    serializer_class = MusicSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsLeaderOrReadOnly]
    filter_backends = [filters.SearchFilter, df_filters.DjangoFilterBackend, filters.OrderingFilter]
    search_fields = ['band__bandname', 'title', 'lyrics']
    filterset_fields = ['category__id']
    ordering_fields = ['like_count__count', 'created_at']
    ordering = ['created_at']

    def perform_create(self, serializer):
        band = Band.objects.filter(leader=self.request.user).first()
        serializer.save(band=band)

    def list(self, request, *args, **kwargs):
        """
        @query_params
        ?random=true
        ランダムでページネーションされたリスト取得
        """
        # =============================================
        # Check: is ?random=true ?
        # =============================================
        if not bool_query_params(request.GET.get('random', None)):
            return super().list(request, *args, **kwargs)

        object_list = list(Music.objects.order_by('?'))
        object_list = object_list[:30]
        serializer = self.get_serializer(object_list, many=True)
        return Response(serializer.data)


class MusicDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    Music を取得、編集、削除。
    """
    queryset = Music.objects.all()
    serializer_class = MusicSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsTheMusicsBandsLeaderOrReadOnly]


class BandMusicsRetrieveView(generics.RetrieveAPIView):
    queryset = Band.objects.all()
    serializer_class = BandMusicsSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        ネストされたページネーションはしない。
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data['musics']
        return Response(data)


class BaseMusicFolderDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    リクエストユーザーのBaseMusicFolderを取得。
    """
    queryset = BaseMusicFolder.objects.all()
    serializer_class = BaseMusicFolderSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self):
        user = self.request.user
        return BaseMusicFolder.objects.filter(user=user)

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset)
        self.check_object_permissions(self.request, obj)
        return obj

    def retrieve(self, request, *args, **kwargs):
        """
        ネストされたページネーションはしない。
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data
        paginated_musics = data.pop('musics')
        data.update(**paginated_musics)
        return Response(data)


class BaseMusicFolderView(generics.ListAPIView):
    queryset = BaseMusicFolder.objects.all()
    serializer_class = BaseMusicFolderSerializer


class PlaylistsView(generics.ListCreateAPIView):
    queryset = Playlist.objects.all()
    serializer_class = PlaylistSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        user = self.request.user
        serializer.save(user=user)


class MyPlaylistsView(generics.ListAPIView):
    queryset = Playlist.objects.all()
    serializer_class = PlaylistSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return Playlist.objects.filter(user=user)


class PlaylistDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Playlist.objects.all()
    serializer_class = PlaylistSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticated, IsOwner])
def like_music(request, pk):
    """
    GET: Musicをすでにlikeしているかどうかをis_liked: Boolの形で返す。
    POST: music=pkかつuser=request.userのLikeを条件に合わせてcreateまたはdeleteする。
    """
    try:
        music = Music.objects.get(pk=pk)
    except Music.DoesNotExist:
        raise NotFound(detail='この音楽は存在しません。')

    if request.method == 'GET':
        if Like.objects.filter(music=music, user=request.user).exists():
            return Response({'is_liked': True}, status=status.HTTP_200_OK)
        else:
            return Response({'is_liked': False}, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        user = request.user
        count = Like.objects.filter(user=user, music=music).count()
        if count == 0:
            like = Like.objects.create(user=user, music=music)
            serializer = LikeSerializer(like)
            # like_countを１足す
            like_count = LikeCount.objects.filter(music=music).first()
            like_count.count = F('count') + 1
            like_count.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            like = Like.objects.filter(user=user, music=music).delete()
            # like_countを１引く
            like_count = LikeCount.objects.filter(music=music).first()
            like_count.count = F('count') - 1
            like_count.save()
            return Response(status=status.HTTP_204_NO_CONTENT)


class MusicLikedUsersView(generics.RetrieveAPIView):
    queryset = Music.objects.all()
    serializer_class = MusicLikedUsersSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        ネストされたページネーションはしない。
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data['liked_users']
        return Response(data)


@api_view(['POST'])
@throttle_classes([AnonAddPlayedCountRateThrottle, UserAddPlayedCountRateThrottle])
def add_played_count(request, pk):
    """
    Musicのplayed_countを+1する
    """
    try:
        music = Music.objects.get(pk=pk)
    except Music.DoesNotExist:
        raise NotFound(detail='この音楽は存在しません。')

    if request.method == 'POST':
        music.played_count = F('played_count') + 1
        music.save()
        music.refresh_from_db()
        serializer = MusicPlayedCountSerializer(music)
        return Response(serializer.data)


# ==============================
# CommentOnMusicViews
# ==============================
class CommentOnMusicView(generics.ListCreateAPIView):
    queryset = CommentOnMusic.objects.all()
    serializer_class = CommentOnMusicSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class MusicCommentOnMusicView(generics.ListCreateAPIView):
    queryset = CommentOnMusic.objects.all()
    serializer_class = CommentOnMusicSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        """
        kwargsから{pk: '112a-afygigayiw-u21hii'}を取得
        """
        kwargs = self.kwargs
        music = None
        music_id = kwargs.get('pk', None)
        try:
            music = Music.objects.get(pk=music_id)
        except Music.DoesNotExist:
            raise NotFound(detail='この音楽は存在しません。')
        return music.comments.all()

    def perform_create(self, serializer):
        kwargs = self.kwargs
        music = None
        music_id = kwargs.get('pk', None)
        try:
            music = Music.objects.get(pk=music_id)
        except Music.DoesNotExist:
            raise NotFound(detail='この音楽は存在しません。')
        serializer.save(user=self.request.user, music=music)


class CommentOnMusicDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = CommentOnMusic.objects.all()
    serializer_class = CommentOnMusicSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]


# ========================================
# Music Category
# ========================================
@api_view(['GET', 'POST', 'DELETE'])
@permission_classes([IsSuperuser])
def initialize_music_categories(request):
    if request.method == 'GET':
        queryset = MusicCategory.objects.all()
        serializer = MusicCategorySerializer(queryset, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        flag = initialize()
        if flag:
            return Response({'status': 'Music Categories were initialized.'})
        return Response({'status': 'Music Categories already exist.'})

    elif request.method == 'DELETE':
        MusicCategory.objects.all().delete()
        return Response({'status': 'All Music Categories were deleted.'})


@api_view(['GET', 'POST'])
@permission_classes([IsSuperuser])
def add_music_category(request):
    if request.method == 'GET':
        queryset = MusicCategory.objects.all()
        serializer = MusicCategorySerializer(queryset, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = MusicCategorySerializer(data=request.data)
        if serializer.is_valid():
            return Response(serializer.data)
