from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.core.files.uploadedfile import TemporaryUploadedFile
from mutagen.mp4 import MP4
from mutagen.mp3 import MP3
from mutagen import MutagenError


def validate_music_file_ext(file: TemporaryUploadedFile) -> None:
    content_type = file.content_type
    if content_type not in ('audio/x-m4a', 'audio/mpeg'):
        raise ValidationError(_('mp3またはm4aの音楽ファイルを選択してください。'))

    ext = file.name.split('.')[-1]
    ext = ext.lower()
    if ext == 'mp3':
        try:
            MP3(file)
        except MutagenError:
            raise ValidationError(_('正しいmp3ファイルを選択してください。'))
    elif ext == 'm4a':
        try:
            MP4(file)
        except MutagenError:
            raise ValidationError(_('正しいm4aファイルを選択してください。'))
    else:
        raise ValidationError(_('mp3またはm4aのファイルを選択してください。'))
