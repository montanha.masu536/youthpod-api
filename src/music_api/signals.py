from youthpod_api.utils import get_music_length
from .models import Playlist, BaseMusicFolder
from django.db.models import F


def post_save_set_length(sender, instance, created, **kwargs):
    """
    Musicがsave()される時に曲の長さを取得してlengthに格納する
    """
    field_file = instance.music

    if created:
        instance.length = get_music_length(field_file)
        instance.save()


def pre_delete_update_num_of_musics(sender, instance, **kwargs):
    """
    Musicが削除される時、そのMusicを持っているPlaylist全てのnum_of_musicsを更新する
    """
    playlists = Playlist.objects.filter(musics=instance.id)
    base_music_folders = BaseMusicFolder.objects.filter(musics=instance.id)
    for p in playlists:
        p.num_of_musics = F('num_of_musics') - 1
        p.save()
    for b in base_music_folders:
        b.num_of_musics = F('num_of_musics') - 1
        b.save()
