from rest_framework import serializers
from user_api.serializers import ProfileSerializer, UserProfileSerializer
from band_api.serializers import PlainBandSerializer
from band_api.models import Band
from .models import Music, Playlist, BaseMusicFolder, Like, LikeCount, CommentOnMusic, MusicCategory
from django.contrib.auth import get_user_model
from youthpod_api.paginators import RelationPaginator
from youthpod_api.utils import bool_query_params


class MusicSerializer(serializers.ModelSerializer):
    band = PlainBandSerializer(read_only=True)
    like_count = serializers.IntegerField(read_only=True, source='like_count.count')
    comment_count = serializers.SerializerMethodField()
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")
    updated_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")
    is_downloaded = serializers.SerializerMethodField()
    is_liked = serializers.SerializerMethodField()

    class Meta:
        model = Music
        fields = ('id', 'band', 'category', 'title', 'music', 'img', 'lyrics', 'length', 'like_count', 'comment_count', 'played_count', 'written_by', 'composed_by', 'created_at', 'updated_at', 'is_downloaded', 'is_liked')
        read_only_fields = ('id', 'band', 'length', 'like_count', 'comment_count', 'played_count', 'created_at', 'updated_at', 'is_downloaded', 'is_liked')
        extra_kwargs = {
            'category': {'required': False},
            'img': {'required': False},
            'lyrics': {'required': False},
        }

    def get_comment_count(self, obj):
        """
        コメントの数を返す
        """
        return obj.comments.all().count()

    def get_is_downloaded(self, obj):
        """
        フロントエンド用にすでにダウンロードしてあるかどうかをBoolで返す
        """
        request = self.context.get('request')
        result = False
        if isinstance(request.user, get_user_model()):
            musics = request.user.base_music_folder.musics.all()
            if obj in musics:
                result = True

        return result

    def get_is_liked(self, obj):
        """
        フロントエンド用にすでにLikeしているかどうかをBoolで返す
        """
        request = self.context.get('request')
        result = False
        if isinstance(request.user, get_user_model()):
            liked_musics = request.user.liked_musics.all()
            if obj in liked_musics:
                result = True

        return result


class MusicLikedUsersSerializer(serializers.ModelSerializer):
    """
    MusicをLikeしたユーザーを全てページネーションで取得する
    """
    liked_users = serializers.SerializerMethodField()

    class Meta:
        model = Music
        fields = ('liked_users',)
        read_only_fields = ('liked_users',)

    def get_liked_users(self, obj):
        queryset = obj.liked_users.all()
        request = self.context.get('request')
        serializer = UserProfileSerializer(queryset, many=True, context={'request': request})
        paginator = RelationPaginator()
        paginated_data = paginator.paginate_queryset(queryset=serializer.data, request=request)
        result = paginator.get_paginated_response(paginated_data)
        return result


class MusicPlayedCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Music
        fields = ('id', 'title', 'played_count')
        read_only_fields = ('id', 'title', 'played_count')


class BaseMusicFolderSerializer(serializers.ModelSerializer):
    """
    params: musics: read_onlyの、playlistに属するmusicのリスト
    params: music_ids: write_onlyの、playlistへの曲を追加、削除などの変更に使う。ex. JSONで{"music_ids": []}をPATCHすれば、BaseMusicFolder.musicsは空になる
    """
    musics = serializers.SerializerMethodField()
    music_ids = serializers.PrimaryKeyRelatedField(queryset=Music.objects.all(), source='musics', many=True, pk_field=serializers.UUIDField(format='hex'), write_only=True)
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = BaseMusicFolder
        fields = ('id', 'user', 'musics', 'music_ids', 'num_of_musics', 'created_at')
        read_only_fields = ('id', 'user', 'musics', 'num_of_musics', 'created_at')

    def get_musics(self, obj):
        """
        query_paramsにno_pagination: trueがあるときページネーションしない
        """
        request = self.context.get('request')
        queryset = obj.musics.all()
        serializer = MusicSerializer(queryset, many=True, context={'request': request})
        no_pagination = bool_query_params(request.query_params.get('no_pagination', None))
        if no_pagination:
            return {'results': serializer.data}
        else:
            paginator = RelationPaginator()
            paginated_data = paginator.paginate_queryset(queryset=serializer.data, request=request)
            result = paginator.get_paginated_response(paginated_data)
            return result

    def update(self, instance, validated_data):
        """
        music_ids(musics)のidのMusicを全てmusicsに格納する。
        playlistを更新した時に合計曲数を取得してnum_of_musicsに格納する。
        PUTのみ
        """
        musics = validated_data.get('musics', None)  # 追加削除するmusics
        if musics:
            for music in musics:
                if music in instance.musics.all():
                    instance.musics.remove(music)
                else:
                    instance.musics.add(music)
        ex_musics = instance.musics.all()
        validated_data['musics'] = ex_musics
        instance = super().update(instance, validated_data)
        instance.num_of_musics = instance.musics.count()
        instance.save()
        return instance


class PlaylistSerializer(serializers.ModelSerializer):
    """
    music_idsのquerysetをrequest.userのmy_musicsに限定できないだろうか。
    一旦validationで追加するmusic_idsがmy_musicsのものであるかどうかをチェックするようにする。
    """
    musics = MusicSerializer(many=True, read_only=True)
    music_ids = serializers.PrimaryKeyRelatedField(queryset=Music.objects.all(), source='musics', many=True, pk_field=serializers.UUIDField(format='hex'), write_only=True)
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = Playlist
        fields = ('id', 'title', 'user', 'musics', 'music_ids', 'num_of_musics', 'total_length', 'is_private', 'created_at')
        read_only_fields = ('id', 'user', 'musics', 'num_of_musics', 'total_length', 'created_at')

    def create(self, validated_data):
        """
        playlistを更新した時に合計曲数を取得してnum_of_musicsに格納する。さらに、合計の曲の秒数もtotal_lengthに格納する。
        """
        instance = super().create(validated_data)
        instance.num_of_musics = instance.musics.count()
        total_music_length = 0
        for m in instance.musics.all():
            total_music_length += m.length
        instance.total_length = total_music_length
        instance.save()
        return instance

    def update(self, instance, validated_data):
        """
        playlistを更新した時に合計曲数を取得してnum_of_musicsに格納する。さらに、合計の曲の秒数もtotal_lengthに格納する。
        """
        musics = validated_data.get('musics', None)  # 追加削除するmusics
        if musics:
            for music in musics:
                if music in instance.musics.all():
                    instance.musics.remove(music)
                else:
                    instance.musics.add(music)
        ex_musics = instance.musics.all()
        validated_data['musics'] = ex_musics
        instance = super().update(instance, validated_data)
        instance.num_of_musics = instance.musics.count()
        total_music_length = 0
        for m in instance.musics.all():
            total_music_length += m.length
        instance.total_length = total_music_length
        instance.save()
        return instance


# ==============================
# Band Music
# ==============================
class BandMusicsSerializer(serializers.ModelSerializer):
    musics = serializers.SerializerMethodField()

    class Meta:
        model = Band
        fields = ('musics',)
        read_only_fields = ('musics',)

    def get_musics(self, obj):
        queryset = obj.musics.all()
        request = self.context.get('request')
        serializer = MusicSerializer(queryset, many=True, context={'request': request})
        paginator = RelationPaginator()
        paginated_data = paginator.paginate_queryset(queryset=serializer.data, request=request)
        result = paginator.get_paginated_response(paginated_data)
        return result


# ==============================
# Like
# ==============================
class LikeSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = Like
        fields = ('user', 'music', 'created')


class LikeCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = LikeCount
        fields = ('music', 'count')


# ==============================
# CommentOnMusic
# ==============================
class CommentOnMusicSerializer(serializers.ModelSerializer):
    user_profile = ProfileSerializer(source='user.profile', read_only=True)
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")
    updated_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = CommentOnMusic
        fields = ('id', 'user_profile', 'comment', 'created_at', 'updated_at')
        read_only_fields = ('id', 'user_profile', 'created_at', 'updated_at')


# ==============================
# Music Category
# ==============================
class MusicCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = MusicCategory
        fields = ('id', 'name',)


class UserLikedMusicsSerializer(serializers.ModelSerializer):
    """
    Userのliked_musicsをread_onlyで取得する
    """
    liked_musics = serializers.SerializerMethodField()

    class Meta:
        model = get_user_model()
        fields = ('liked_musics',)
        read_only_fields = ('liked_musics',)

    def get_liked_musics(self, obj):
        queryset = obj.liked_musics.all()
        request = self.context.get('request')
        serializer = MusicSerializer(queryset, many=True, context={'request': request})
        paginator = RelationPaginator()
        paginated_data = paginator.paginate_queryset(queryset=serializer.data, request=request)
        result = paginator.get_paginated_response(paginated_data)
        return result
