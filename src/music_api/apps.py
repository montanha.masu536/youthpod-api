from django.apps import AppConfig


class MusicApiConfig(AppConfig):
    name = 'music_api'

    def ready(self):
        from django.db.models.signals import post_save, pre_delete
        from .signals import post_save_set_length, pre_delete_update_num_of_musics
        from .models import Music
        post_save.connect(post_save_set_length, sender=Music)
        pre_delete.connect(pre_delete_update_num_of_musics, sender=Music)
