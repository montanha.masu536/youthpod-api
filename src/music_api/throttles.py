from rest_framework.throttling import SimpleRateThrottle


class AnonAddPlayedCountRateThrottle(SimpleRateThrottle):
    """
    対象：ログインしてないユーザー
    対象API：add_played_count
    Rate：10/day
    """
    scope = 'anon_add_played_count'

    def get_cache_key(self, request, view):
        if request.user.is_authenticated:
            return None  # Only throttle unauthenticated requests.

        return self.cache_format % {
            'scope': self.scope,
            'ident': self.get_ident(request)
        }


class UserAddPlayedCountRateThrottle(SimpleRateThrottle):
    """
    対象：ログインしているユーザー
    対象API：add_played_count
    Rate：50/day
    """
    scope = 'user_add_played_count'

    def get_cache_key(self, request, view):
        if request.user.is_authenticated:
            ident = request.user.pk
        else:
            ident = self.get_ident(request)

        return self.cache_format % {
            'scope': self.scope,
            'ident': ident
        }
