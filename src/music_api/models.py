import uuid
from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import validate_image_file_extension
from .validators import validate_music_file_ext
from youthpod_api.validators import validate_image_size, validate_music_file

from datetime import datetime


def music_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    date = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
    return f'music_api/music_files/band_{instance.band.id}/{instance.title[:5]}_{date}.{ext}'


def image_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    date = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
    return f'music_api/images/music_images/band_{instance.band.id}/{instance.title[:5]}_{date}.{ext}'


class MusicManager(models.Manager):
    """
    Musicオブジェクト作成と同時に、それに紐づくLikeCountオブジェクトも作成する。
    """
    def create(self, *args, **kwargs):
        music = Music(*args, **kwargs)
        music.save()
        like_count = LikeCount(music=music, count=0)
        like_count.save()
        return music


class Music(models.Model):
    """
    Music モデル
    categoryに関しては、ver2でTag機能を追加して種類を豊富にする
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    band = models.ForeignKey("band_api.Band", related_name='musics', on_delete=models.CASCADE)
    category = models.ForeignKey("music_api.MusicCategory", null=True, related_name='music_category', on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    music = models.FileField(upload_to=music_upload_to, max_length=255, validators=[validate_music_file_ext, validate_music_file])
    img = models.ImageField(upload_to=image_upload_to, max_length=255, validators=[validate_image_file_extension, validate_image_size])
    lyrics = models.TextField(max_length=2000, null=True, blank=True)
    written_by = models.CharField(null=True, blank=True, max_length=50)
    composed_by = models.CharField(null=True, blank=True, max_length=50)
    length = models.IntegerField(null=True, blank=True)
    liked_users = models.ManyToManyField(get_user_model(), through='Like', related_name="liked_musics")
    played_count = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = MusicManager()

    class Meta:
        ordering = ['updated_at']

    def __str__(self):
        return str(self.id)


class BaseMusicFolder(models.Model):
    """
    UserがMusicをダウンロードした時にそれが保存される場所。Userは必ず一つのみBaseMusicFolderを持つ。
    params: num_of_musics: musicsの総数
    """
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name="base_music_folder")
    musics = models.ManyToManyField(Music, related_name="base_music_folder")
    num_of_musics = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_at']


class Playlist(models.Model):
    """
    params: num_of_musics: musicsの総数
    params: total_length: musicsの合計の曲の長さ。曲を追加するたびに計上していく。
    memo: is_privateなどを追加してもいいかもしれない。
    """
    title = models.CharField(max_length=30)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name="playlists")
    musics = models.ManyToManyField(Music)
    num_of_musics = models.IntegerField(default=0)
    total_length = models.IntegerField(default=0)
    is_private = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_at']


class Like(models.Model):
    """
    いいねオブジェクト。
    """
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    music = models.ForeignKey(Music, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return self.user


class LikeCount(models.Model):
    """
    Likeオブジェクトが、
    createで+1、
    deleteで-1するように上書きする。
    """
    music = models.OneToOneField(Music, related_name='like_count', on_delete=models.CASCADE, primary_key=True)
    count = models.IntegerField()

    def __str__(self):
        return str(self.count)


class CommentOnMusic(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='comments_on_music')
    music = models.ForeignKey(Music, on_delete=models.CASCADE, related_name='comments')
    comment = models.TextField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']


class MusicCategory(models.Model):
    """
    Music用カテゴリーモデル。あくまで検索目的で、音楽の見た目としてはあまり表示することはしない。
    """
    id = models.CharField(primary_key=True, unique=True, max_length=255)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.id
