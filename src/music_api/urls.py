from django.urls import path, include, re_path
from .views import MusicView, MusicDetailView, BaseMusicFolderDetailView, BaseMusicFolderView, PlaylistsView, MyPlaylistsView, PlaylistDetailView, like_music, MusicLikedUsersView, add_played_count, CommentOnMusicDetailView, MusicCommentOnMusicView, initialize_music_categories, add_music_category


musics_urlpatterns = [
    # Music
    path('', MusicView.as_view(), name='music_list_create'),
    re_path(r'(?P<pk>[\w-]{36})/$', MusicDetailView.as_view(), name='music_detail'),
    # Like
    re_path(r'(?P<pk>[\w-]{36})/actions/like/$', like_music, name='music_like'),
    re_path(r'(?P<pk>[\w-]{36})/actions/add_played_count/$', add_played_count, name='music_add_played_count'),
    re_path(r'(?P<pk>[\w-]{36})/liked_users/$', MusicLikedUsersView.as_view(), name='music_detail_liked_users'),
    # CommentOnMusic
    path('comments/<int:pk>/', CommentOnMusicDetailView.as_view(), name='comment_on_music_detail'),
    re_path(r'(?P<pk>[\w-]{36})/comments/$', MusicCommentOnMusicView.as_view(), name='music_comment_on_music_list_create'),
]

playlists_urlpatterns = [
    # BaseMusicFolder
    path('base_music_folders/', BaseMusicFolderView.as_view(), name='bmf_list_create'),
    path('my/library/', BaseMusicFolderDetailView.as_view(), name='my_library'),
    # Playlist
    path('', PlaylistsView.as_view(), name='playlist_list_create'),
    path('<int:pk>/', PlaylistDetailView.as_view(), name='playlist_detail'),
    path('my/playlists/', MyPlaylistsView.as_view(), name='my_playlists')
]

urlpatterns = [
    path('musics/', include(musics_urlpatterns)),
    path('playlists/', include(playlists_urlpatterns)),
]

# Music Category Initializing and Adding for superuser
urlpatterns += [
    path('music_categories/initialize/', initialize_music_categories),
    path('music_categories/', add_music_category),
]
