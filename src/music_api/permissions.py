from rest_framework import permissions
from user_api.decorators import allow_superuser


class IsTheMusicsBandsLeaderOrReadOnly(permissions.BasePermission):
    """
    MusicはそのBandのleaderのみが編集削除可能。
    """
    @allow_superuser
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.band.leader == request.user
