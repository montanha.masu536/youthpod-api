from django.db.models.fields.files import FieldFile

from mutagen.mp4 import MP4
from mutagen.mp3 import MP3

from youthpod_api.constants import MUSIC_CATEGORIES_CHILDREN, MUSIC_CATEGORIES_CHILDREN_IDS, MUSIC_CATEGORIES_PARENTS, MUSIC_CATEGORIES_PARENTS_IDS

from music_api.models import MusicCategory

import random


def get_music_length(file: FieldFile) -> int:
    """
    MP3またはM4Aファイルから曲の長さを取得する。
    """
    length = None
    with file.open(mode='rb') as fd:
        ext = file.name.split('.')[-1]
        ext = ext.lower()
        if ext == 'mp3':
            length = int(MP3(fd).info.length)
        elif ext == 'm4a':
            length = int(MP4(fd).info.length)

    return length


def error_data(status: int, errors: dict) -> dict:
    """
    関数ViewなどでError Responseをする場合に使用。
    """
    assert isinstance(errors, dict)
    error = {"status_code": status, "errors": errors}
    return error


def initialize_music_categories() -> bool:
    """
    MusicCategoryのデータを初期化する。
    youthpod_api.constantsの設定を変えることでカテゴリーを変更できる
    """
    if not MusicCategory.objects.all().exists():
        for p in zip(MUSIC_CATEGORIES_PARENTS_IDS, MUSIC_CATEGORIES_PARENTS):
            MusicCategory.objects.create(id=p[0], name=p[1])
        print('Music Categories were initialized.')
        return True
    print('Music Categories already exist.')
    return False


def initialize_music_categories_with_children():
    """
    < Deprecated >
    MusicCategoryのデータを初期化する。
    youthpod_api.constantsの設定を変えることでカテゴリーを変更できる
    """
    parent_array = []
    for p in zip(MUSIC_CATEGORIES_PARENTS_IDS, MUSIC_CATEGORIES_PARENTS):
        parent = MusicCategory.objects.create(id=p[0], name=p[1])
        parent_array.append(parent)
    for i, cid_c in enumerate(zip(MUSIC_CATEGORIES_CHILDREN_IDS, MUSIC_CATEGORIES_CHILDREN)):
        a, b = cid_c
        for id_n_p in zip(a, b):
            parent = parent_array[i]
            MusicCategory.objects.create(id=parent.id + '__' + id_n_p[0], name=parent.name + '/' + id_n_p[1], parent=parent)


def shuffle_dict(d: dict) -> dict:
    temp = list(d.values())
    random.shuffle(temp)

    return dict(zip(d, temp))


def bool_query_params(param) -> bool:
    """
    query_paramsの真偽値をPythonのboolに変換する
    """
    if param == 'true' or param == 1:
        return True
    return False
