from django.urls import path, include, re_path
from django.conf.urls.static import static
from django.conf import settings
from user_api.views import UserActivationView, PassWordResetConfirmView

urlpatterns = [
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.jwt')),
    # (?<name>...)について -> https://docs.python.org/ja/3/library/re.html , [\w-]+ はなんらかの文字列を意味する
    re_path(r'^activate/(?P<uid>[\w-]+)/(?P<token>[\w-]+)/$', UserActivationView.as_view()),
    re_path(r'^password/reset/confirm/(?P<uid>[\w-]+)/(?P<token>[\w-]+)/$', PassWordResetConfirmView.as_view()),
    path('api/v1/', include('user_api.urls')),
    path('api/v1/', include('band_api.urls')),
    path('api/v1/', include('music_api.urls')),
    path('api/v1/admin_background/', include('admin_background.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
