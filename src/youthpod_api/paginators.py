from rest_framework import pagination


class RelationPaginator(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        return {
            'count': self.page.paginator.count,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'results': data
        }
