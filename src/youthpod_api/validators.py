"""
Common validators
"""
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.core.files.uploadedfile import TemporaryUploadedFile
import re


def validate_file_size(file: TemporaryUploadedFile, size_mb: int = 2) -> None:
    """
    size_mb以下のファイルのみアップロード可能
    ファイルサイズ(MB)は小数点第３位で四捨五入
    """
    mb = round(file.size / 1024**2, 3)
    if mb > size_mb:
        raise ValidationError(_("ファイルサイズが大きすぎます。" + str(size_mb) + "MB以下のファイルを選択してください。"))


def validate_image_size(file):
    """ImageField用のバリデータ"""
    validate_file_size(file)


def validate_music_file(file):
    """FileField用(Music)のバリデータ"""
    validate_file_size(file, 15)


def validate_alphabet_number_underscore(value):
    """
    アルファベット小文字、半角数字または_のみ許可
    """
    regex = re.compile(r'^[a-zA-Z0-9_]+$', re.DOTALL)
    match = re.match(regex, value)
    if match is None:
        raise ValidationError(_('小文字・大文字アルファベット[a-z, A-Z]、半角数字[0-9]、またはアンダーバー[_]を使用してください。'))
