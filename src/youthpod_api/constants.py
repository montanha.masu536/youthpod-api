# YOUTHPOD_CONSTANTS
GENERAL_ERROR_MESSAGE = {'non_field_errors': 'エラーが発生しました。お手数ですが、開発チームにお問い合わせください。'}
MEMBER_ALREADY_EXISTS = {'request_id': 'このIDのユーザーはすでに招待されているか、バンドに所属しています。'}
INVALID_REQUEST_ID = {'request_id': '正しいIDを入力してください。'}

# Categories
MUSIC_CATEGORIES_PARENTS = ['弾き語りカバー曲', '弾き語りオリジナル曲', 'バンドカバー曲', 'バンドオリジナル曲', 'ボーカロイド曲', 'その他']

MUSIC_CATEGORIES_PARENTS_IDS = ['solo_cover', 'solo_original', 'band_cover', 'band_original', 'vocaloid', 'others']

MUSIC_CATEGORIES_CHILDREN = [
    ['ピアノ', 'ギター', 'ウクレレ', 'Instrumental', 'その他'],
    ['ピアノ', 'ギター', 'ウクレレ', 'Instrumental', 'その他'],
    ['ロック', 'ジャズ', 'メタル', '吹奏楽など', 'Instrumental', 'その他'],
    ['ロック', 'ジャズ', 'メタル', '吹奏楽など', 'Instrumental', 'その他'],
    ['Instrumental'],
    ['ソロ演奏', '複数人演奏', 'アカペラ', 'その他']
]

MUSIC_CATEGORIES_CHILDREN_IDS = [
    ['piano', 'guitar', 'ukulele', 'instrumental', 'others'],
    ['piano', 'guitar', 'ukulele', 'instrumental', 'others'],
    ['rock', 'jazz', 'metal', 'brassband', 'instrumental', 'others'],
    ['rock', 'jazz', 'metal', 'brassband', 'instrumental', 'others'],
    ['instrumental'],
    ['solo', 'group', 'akapela', 'others']
]
