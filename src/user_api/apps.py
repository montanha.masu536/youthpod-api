from django.apps import AppConfig


class UserApiConfig(AppConfig):
    name = 'user_api'

    def ready(self):
        from django.db.models.signals import post_save
        from .signals import post_save_create_base_music_folder
        from django.contrib.auth import get_user_model
        post_save.connect(post_save_create_base_music_folder, sender=get_user_model())
