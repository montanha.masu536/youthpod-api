from rest_framework import permissions
from .models import Profile
from user_api.decorators import allow_superuser


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `user` attribute.
    """

    @allow_superuser
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `user`.
        return obj.user == request.user


class IsOwner(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    """

    @allow_superuser
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class IsNotOwner(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    """

    @allow_superuser
    def has_object_permission(self, request, view, obj):
        return not obj.user == request.user


class IsRequestedUserOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `user` attribute.
    """

    @allow_superuser
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `user`.
        return obj == request.user


class IsRequestedUser(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    """

    @allow_superuser
    def has_object_permission(self, request, view, obj):
        return obj == request.user


class DoesNotHaveAnyProfileOrReadOnly(permissions.BasePermission):
    """
    To POST, requested user must not have Profile model.
    """
    message = 'すでにプロフィールがあります。by YouthPod team.'

    @allow_superuser
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return not Profile.objects.filter(user=request.user).exists()


class IsSuperuser(permissions.BasePermission):
    """
    superuserのみ許可
    """
    @allow_superuser
    def has_permission(self, request, view):
        return False
