from music_api.models import BaseMusicFolder


def post_save_create_base_music_folder(sender, instance, created, **kwargs):
    """
    Userが作成された時、同時にBaseMusicFolderも作成する。
    """

    if created:
        base_music_folder = BaseMusicFolder(user=instance)
        base_music_folder.save()
        instance.save()
