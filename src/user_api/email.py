from djoser import email


class ActivationEmail(email.ActivationEmail):
    template_name = "user_api/email/activation.html"


class ConfirmationEmail(email.ConfirmationEmail):
    template_name = "user_api/email/confirmation.html"


class PasswordResetEmail(email.PasswordResetEmail):
    template_name = "user_api/email/password_reset.html"


class PasswordChangedConfirmationEmail(email.PasswordChangedConfirmationEmail):
    template_name = "user_api/email/password_changed_confirmation.html"
