def allow_superuser(func):
    """
    is_superuserがTrueのユーザーは管理ユーザーとして全てのアクションを行うことができる。
    has_permission、または、has_object_permissionにこのデコレータをつけて使用する。
    """
    def wrapper(self, request, *args):
        result = func(self, request, *args)
        if request.user.is_superuser:
            result = True
        return result
    return wrapper
