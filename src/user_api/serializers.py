from rest_framework import serializers
from .models import Profile
from django.contrib.auth import get_user_model


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'profile', 'request_id')


class UserRequestIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'request_id')


class ProfileSerializer(serializers.ModelSerializer):
    """
    band_idの変更は、中間テーブル生成時に自動変更する。(Manager)
    urlはテストようのため削除推奨。
    """

    class Meta:
        model = Profile
        fields = ('user', 'username', 'img', 'band_id')
        read_only_fields = ('user', 'band_id',)


class UserProfileSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = get_user_model()
        fields = ('profile',)
        read_only_fields = ('profile',)


class MyProfileSerializer(serializers.ModelSerializer):
    """
    myprofile用のSerializer
    """
    user_request_id = serializers.CharField(max_length=16, min_length=8, source='user.request_id')

    class Meta:
        model = Profile
        fields = ('user', 'username', 'img', 'band_id', 'user_request_id')
        read_only_fields = ('user', 'band_id')

    def update(self, instance, validated_data):
        """
        ProfileのSerializerからUserのrequest_idを変更する。
        """
        instance.username = validated_data.get('username', instance.username)
        instance.img = validated_data.get('img', instance.img)
        instance.save()
        # Userへの処理
        user = instance.user
        user_data = validated_data.get('user', {})
        user.request_id = user_data.get('request_id', user.request_id)
        user.save()
        return instance
