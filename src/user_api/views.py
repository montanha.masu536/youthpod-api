from .serializers import ProfileSerializer, UserSerializer, UserRequestIdSerializer, MyProfileSerializer
from music_api.serializers import UserLikedMusicsSerializer
from band_api.serializers import UserFavoriteBandsSerializer
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.exceptions import NotFound
from band_api.permissions import IsNotLeader
from .permissions import IsOwnerOrReadOnly, IsRequestedUser, DoesNotHaveAnyProfileOrReadOnly, IsSuperuser
from django.contrib.auth import get_user_model
from .models import Profile
import requests
from youthpod_api.utils import error_data

# Function-based View
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.response import Response


# ==============================
# Djoser Views
# ==============================
class UserActivationView(APIView):
    renderer_classes = [TemplateHTMLRenderer]

    def get(self, request, uid, token):
        protocol = 'https://' if request.is_secure() else 'http://'
        web_url = protocol + request.get_host()
        post_url = web_url + "/auth/users/activation/"
        post_data = {'uid': uid, 'token': token}
        result = requests.post(post_url, data=post_data)
        content = result.text
        if len(content) != 0:
            text = "このURLの使用期限が切れています。アプリから再度認証メールを送信してください。"
        else:
            text = "認証に成功しました。アプリに戻ってYouthPodをお楽しみください！"
        return Response({'text': text}, template_name='user_api/result.html')


class PassWordResetConfirmView(APIView):
    renderer_classes = [TemplateHTMLRenderer]

    def get(self, request, uid, token):
        protocol = 'https://' if request.is_secure() else 'http://'
        web_url = protocol + request.get_host()
        post_url = web_url + "/auth/users/reset_password_confirm/"
        context = {'post_url': post_url, 'uid': uid, 'token': token}
        return Response(context, template_name='user_api/password_reset_page.html')


# ==============================
# Main user_api Views
# ==============================
class UserListView(generics.ListAPIView):
    """
    Userのリストを取得。プロダクトレベルでは使用しない。
    """
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsSuperuser]


class ProfileListCreateView(generics.ListCreateAPIView):
    """
    主にProfile作成に使用。
    """
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, DoesNotHaveAnyProfileOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ProfileRetrieveView(generics.RetrieveAPIView):
    """
    Profileの詳細。
    """
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class MyProfileDetailView(generics.RetrieveUpdateAPIView):
    queryset = Profile.objects.all()
    serializer_class = MyProfileSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]


@api_view(['GET', 'PATCH'])
@permission_classes([IsAuthenticated, IsOwnerOrReadOnly])
def get_update_myprofile(request):
    """
    url: /api/myprofile/
    リクエストユーザーのプロフィールを取得し、編集ができる。
    PATCH: jsonで更新したいデータのみをPATCHしてプロフィールを編集する。これは、画像が変更されないようにするためにしている。また、usernameだけを変えたいとか、一部更新をしたい需要の方が圧倒的に高いだろうということもある。
    """
    profile = Profile.objects.filter(user=request.user).first()
    if profile is None:
        raise NotFound(detail='このユーザーはプロフィールを持っていません。')

    if request.method == 'GET':
        serializer = MyProfileSerializer(profile)
        return Response(serializer.data, status=status.HTTP_200_OK)

    elif request.method == 'PATCH':
        serializer = MyProfileSerializer(profile, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(error_data(400, serializer.errors), status=status.HTTP_400_BAD_REQUEST)
    return Response({'error': 'Only GET and PATCH are allowed.'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PATCH'])
@permission_classes([IsAuthenticated])
def register_request_id(request):
    """
    Userのrequest_idを設定する
    """
    if request.method == 'GET':
        serializer = UserRequestIdSerializer(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'PATCH':
        serializer = UserRequestIdSerializer(request.user, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_206_PARTIAL_CONTENT)
        return Response(error_data(400, serializer.errors), status=status.HTTP_400_BAD_REQUEST)
    return Response({'error': 'Only GET and PATCH are allowed.'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def has_profile(request):
    """
    GET: request.userがprofileを持っているかどうかを返す
    iOSクライエント上で画面遷移の挙動を制御するために使われる
    """
    if request.method == 'GET':
        flag = None
        try:
            request.user.profile
            flag = True
        except Profile.DoesNotExist:
            flag = False
        return Response({"has_profile": flag}, status=status.HTTP_200_OK)
    return Response({'error': 'Only GET is allowed.'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
@permission_classes([IsAuthenticated, IsRequestedUser, IsNotLeader])
def get_delete_user(request):
    """
    Userを削除する
    """
    if request.method == 'GET':
        serializer = UserRequestIdSerializer(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'DELETE':
        request.user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    return Response({'error': 'Only DELETE is allowed.'}, status=status.HTTP_400_BAD_REQUEST)


class UserLikedMusicsView(generics.RetrieveAPIView):
    """
    UserがいいねしたMusicを全て取得する
    """
    queryset = get_user_model().objects.all()
    serializer_class = UserLikedMusicsSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        ネストされたページネーションはしない
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data['liked_musics']
        return Response(data)


class UserFavoriteBandsView(generics.RetrieveAPIView):
    """
    UserがいいねしたMusicを全て取得する
    """
    queryset = get_user_model().objects.all()
    serializer_class = UserFavoriteBandsSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        ネストされたページネーションはしない
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data['favorite_bands']
        return Response(data)
