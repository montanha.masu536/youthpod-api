from django.urls import path, include
from .views import UserListView, ProfileListCreateView, get_update_myprofile, register_request_id, ProfileRetrieveView, has_profile, get_delete_user, UserLikedMusicsView, UserFavoriteBandsView


users_urlpatterns = [
    path('', UserListView.as_view(), name='user_list'),
    path('<int:pk>/liked_musics/', UserLikedMusicsView.as_view(), name='user_detail_liked_musics'),
    path('<int:pk>/favorite_bands/', UserFavoriteBandsView.as_view(), name='user_detail_favorite_bands'),
    path('register_request_id/', register_request_id, name='register_request_id'),
    path('has_profile/', has_profile, name='has_profile'),
    path('actions/delete/', get_delete_user, name='delete_user'),
    path('my/profile/', get_update_myprofile, name='my_profile'),
]

profiles_urlpatterns = [
    path('', ProfileListCreateView.as_view(), name='profile_list_create'),
    path('<int:pk>/', ProfileRetrieveView.as_view(), name='profile_detail'),
]

urlpatterns = [
    path('users/', include(users_urlpatterns)),
    path('profiles/', include(profiles_urlpatterns)),
]
