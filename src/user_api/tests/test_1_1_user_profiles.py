from django.test import TransactionTestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient
from rest_framework import status
from user_api.models import Profile


class AuthorizedUserApiTests(TransactionTestCase):
    """
    状況:
    認証済みユーザーがProfileを持っていない
    もう一つのダミーユーザー(id:1)と、ダミープロフィール(id:1)が存在
    """
    reset_sequences = True

    def setUp(self):
        # urls
        self.url_profile_list_create = reverse('profile_list_create')
        self.url_profile_detail_1 = '/api/v1/profiles/1/'
        self.url_my_profile = '/api/v1/users/my/profile/'
        self.url_has_profile = reverse('has_profile')
        # dummies
        self.dum_user = get_user_model().objects.create_user(email='dummy@dummy.com', password='dummy123TTT')
        self.dum_profile = Profile.objects.create(user=self.dum_user, username='dummy')
        self.user = get_user_model().objects.create_user(email='montanha.masu536@gmail.com', password='Awesome123TTT')
        # others
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    def test_1_1_1_should_get_all_profiles(self):
        res = self.client.get(self.url_profile_list_create)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [
                {
                    "user": 1,
                    "username": self.dum_profile.username,
                    "img": None,
                    "band_id": self.dum_profile.band_id
                }
            ]
        })

    def test_1_1_2_should_create_profile(self):
        payload = {
            'username': 'dummy'
        }
        res = self.client.post(self.url_profile_list_create, payload)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_1_1_3_should_get_profile_detail_with_id_1(self):
        res = self.client.get(self.url_profile_detail_1)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, {
            "user": 1,
            "username": self.dum_profile.username,
            "img": None,
            "band_id": self.dum_profile.band_id
        })

    def test_1_1_4_should_update_profile_that_is_created_by_user(self):
        payload_POST = {
            'username': 'dummy'
        }
        payload_PATCH = {
            'username': 'updated_dummy'
        }
        self.client.post(self.url_profile_list_create, payload_POST)
        res = self.client.patch(self.url_my_profile, payload_PATCH)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, {
            "user": 2,
            "username": "updated_dummy",
            "img": None,
            "band_id": self.user.profile.band_id,
            "user_request_id": None
        })

    def test_1_1_5_should_NOT_return_true_when_get_has_profile(self):
        res = self.client.get(self.url_has_profile)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, {
            "has_profile": False
        })


class AuthorizedUserProfileAlreadyCreatedApiTests(TransactionTestCase):
    """
    状況:
    認証済みユーザーがProfileを既に持っている
    """
    reset_sequences = True

    def setUp(self):
        # urls
        self.url_my_profile = '/api/v1/users/my/profile/'
        self.url_register_request_id = reverse('register_request_id')
        self.url_has_profile = reverse('has_profile')
        self.url_delete_user = reverse('delete_user')
        # dummies
        self.user = get_user_model().objects.create_user(email='dummy@dummy.com', password='dummypsw_123')
        self.profile = Profile.objects.create(user=self.user, username='dummy')
        # others
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    def test_1_1_6_should_register_request_id(self):
        payload = {
            "request_id": "hakumizuki"
        }
        res = self.client.patch(self.url_register_request_id, payload)
        self.assertEqual(res.status_code, status.HTTP_206_PARTIAL_CONTENT)
        self.assertEqual(res.data, {
            "id": 1,
            "request_id": "hakumizuki"
        })

    def test_1_1_7_should_get_my_profile(self):
        res = self.client.get(self.url_my_profile)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, {
            "user": 1,
            "username": "dummy",
            "img": None,
            "band_id": None,
            "user_request_id": None
        })

    def test_1_1_8_should_return_true_when_get_has_profile(self):
        res = self.client.get(self.url_has_profile)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, {
            "has_profile": True
        })

    def test_1_1_9_should_delete_user(self):
        res = self.client.delete(self.url_delete_user)
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)


class UnAuthorizedUserApiTests(TransactionTestCase):
    """
    状況:
    ユーザーがログインしていない場合
    """
    reset_sequences = True

    def setUp(self):
        # urls
        self.url_profile_list_create = reverse('profile_list_create')
        self.url_profile_detail_1 = '/api/v1/profiles/1/'
        self.url_my_profile = '/api/v1/users/my/profile/'
        self.url_register_request_id = reverse('register_request_id')
        self.url_has_profile = reverse('has_profile')
        # dummies
        self.dum_user = get_user_model().objects.create_user(email='dummy@dummy.com', password='dummy123TTT')
        self.dum_profile = Profile.objects.create(user=self.dum_user, username='dummy')
        # others
        self.client = APIClient()

    def test_1_1_10_should_be_401_UNAUTHORIZED_when_post_to_create_profile(self):
        payload = {
            "username": "dummy"
        }
        res = self.client.post(self.url_profile_list_create, payload)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_1_1_11_should_be_401_UNAUTHORIZED_when_get_my_profile(self):
        res = self.client.get(self.url_my_profile)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_1_1_12_should_be_401_UNAUTHORIZED_when_register_request_id(self):
        payload = {
            "request_id": "hakumizuki"
        }
        res = self.client.patch(self.url_register_request_id, payload)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_1_1_13_should_be_401_UNAUTHORIZED_when_get_has_profile(self):
        res = self.client.get(self.url_has_profile)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_1_1_14_should_get_all_profiles(self):
        res = self.client.get(self.url_profile_list_create)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
