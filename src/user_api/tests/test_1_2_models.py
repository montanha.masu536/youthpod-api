from django.test import TransactionTestCase
from django.contrib.auth import get_user_model
from user_api.models import Profile


class AuthorizedUserApiTests(TransactionTestCase):
    """
    CustomUserモデルとProfileモデルのcreate, deleteのテスト
    """
    reset_sequences = True

    def test_1_2_NUM_create_CustomUser_object_correctly(self):
        get_user_model().objects.create(email='dummy@gmail.com', password='dummy123')
        user = get_user_model().objects.first()
        self.assertEqual(user.id, 1)
        self.assertEqual(user.email, 'dummy@gmail.com')
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.is_superuser, False)

    def test_1_2_NUM_create_Profile_object_correctly(self):
        get_user_model().objects.create(email='dummy@gmail.com', password='dummy123')
        user = get_user_model().objects.first()
        Profile.objects.create(user=user, username='dummyuser')
        profile = Profile.objects.first()
        self.assertEqual(profile.user, user)
        self.assertEqual(profile.username, 'dummyuser')
        self.assertEqual(profile.band_id, None)
