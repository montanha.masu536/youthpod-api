from django.db import models
from django.contrib.auth.models import BaseUserManager, PermissionsMixin, AbstractBaseUser
from django.contrib.auth import get_user_model
from django.core.validators import MinLengthValidator, validate_image_file_extension
from youthpod_api.validators import validate_image_size, validate_alphabet_number_underscore


def img_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    return f'user_api/profile_images/user_{instance.user.id}.{ext}'


class CustomUserManager(BaseUserManager):
    """
    CustomUserManager: デフォルトのUserモデルを上書きし、email, passwordによる認証に変更するために作成。
    """
    def create_user(self, email, password=None):
        """
        Creates and saves a user with the given email, and password.
        """
        if not email:
            raise ValueError('User must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        """
        Creates and saves a superuser with the given email, and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_superuser = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    emailをメイン認証として使用。
    request_idはバンドメンバーの認証に使用。(招待機能)
    """
    email = models.EmailField(
        max_length=255,
        unique=True,
    )
    request_id = models.CharField(unique=True, null=True, blank=True, max_length=16, validators=[validate_alphabet_number_underscore, MinLengthValidator(8)])
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email


class Profile(models.Model):
    """
    ユーザー情報を保存するモデル。
    作成のタイミングは、User作成の直後。
    ログイン後にリクエストするため、user=request.userになる。この方がバリデーションがしやすい。
    """
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name='profile', primary_key=True)
    username = models.CharField(max_length=16)
    img = models.ImageField(upload_to=img_upload_to, max_length=255, null=True, blank=True, validators=[validate_image_file_extension, validate_image_size])
    band_id = models.CharField(null=True, max_length=50)

    class Meta:
        ordering = ['username']

    def __str__(self):
        return self.username
