from rest_framework import generics
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from user_api.permissions import IsOwnerOrReadOnly, IsOwner, IsNotOwner, IsSuperuser
from .permissions import DoesNotHaveAnyBandOrReadOnly, IsLeaderOrReadOnly, IsTheBandsLeaderOrReadOnly, IsTheBandsLeader, IsLeader, IsNotLeaderButMember
from .serializers import BandSerializer, BandMemberSerializer, MemberRequestSerializer, RequestIdSerializer, LiveInfoSerializer, BandLiveInfoSerializer, CommentOnLiveInfoSerializer, BandFansSerializer, PaginatedBandFansSerializer, MyBandSerializer
from .models import Band, BandMember, LiveInfo, CommentOnLiveInfo, BandFans, BandFansCount
from django.db.models import F
# Function-based View
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.response import Response
from rest_framework.exceptions import NotFound
# utils
from youthpod_api import constants
from youthpod_api.utils import error_data, bool_query_params
# filters
from rest_framework import filters
from django_filters import rest_framework as df_filters


# ==============================
# Band Views
# ==============================
class BandListCreateView(generics.ListCreateAPIView):
    queryset = Band.objects.all()
    serializer_class = BandSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, DoesNotHaveAnyBandOrReadOnly]
    filter_backends = [filters.SearchFilter, df_filters.DjangoFilterBackend, filters.OrderingFilter]
    search_fields = ['bandname', 'region', 'leader__profile__username']
    filterset_fields = ['is_active']
    ordering_fields = ['created_at', 'total_likes', 'hiatus_date', 'num_of_musics', 'fans_count']
    ordering = ['updated_at']

    def perform_create(self, serializer):
        serializer.save(leader=self.request.user)

    def list(self, request, *args, **kwargs):
        """
        @query_params
        ?random=true
        ランダムでページネーションされたリスト取得
        """
        # =============================================
        # Check: is ?random=true ?
        # =============================================
        if not bool_query_params(request.GET.get('random', None)):
            return super().list(request, *args, **kwargs)

        object_list = list(Band.objects.order_by('?'))
        object_list = object_list[:30]
        serializer = self.get_serializer(object_list, many=True)
        return Response(serializer.data)


class BandRetrieveView(generics.RetrieveAPIView):
    queryset = Band.objects.all()
    serializer_class = BandSerializer


class BandRetrieveDestroyView(generics.RetrieveDestroyAPIView):
    """
    Band削除。
    """
    queryset = Band.objects.all()
    serializer_class = BandSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsTheBandsLeader]


class MyBandDetailView(generics.RetrieveUpdateAPIView):
    """
    リクエストユーザーのBandを取得、編集。
    """
    queryset = Band.objects.all()
    serializer_class = MyBandSerializer
    permission_classes = [IsAuthenticated, IsTheBandsLeaderOrReadOnly]

    def get_queryset(self):
        user = self.request.user
        return Band.objects.filter(members__user=user)

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset)
        self.check_object_permissions(self.request, obj)
        return obj


# ==============================
# BandMember Views
# ==============================
class BandMemberListCreateView(generics.ListCreateAPIView):
    queryset = BandMember.objects.all()
    serializer_class = BandMemberSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsLeaderOrReadOnly]
    pagination_class = None

    def perform_create(self, serializer):
        """
        自動的にleaderのBandを紐付け。
        """
        band = Band.objects.filter(leader=self.request.user).first()
        serializer.save(band=band)


class BandMemberSetIsLeaderView(generics.RetrieveUpdateAPIView):
    queryset = BandMember.objects.all()
    serializer_class = BandMemberSerializer
    permission_classes = [IsAuthenticated, IsLeader, IsNotOwner]

    def perform_update(self, serializer):
        """
        Leader権を譲渡すると同時に自分のis_leaderをFalseにする。
        """
        leader = BandMember.objects.filter(user=self.request.user).first()
        leader.is_leader = False
        leader.save()
        serializer.save()


@api_view(['POST'])
@permission_classes([IsAuthenticated, IsLeader])
def create_band_member_with_request_id(request):
    """
    リーダーがバンドメンバーを招待する。
    """
    if request.method == 'POST':
        band = Band.objects.filter(leader=request.user).first()
        if band is None:
            return Response(error_data(400, constants.GENERAL_ERROR_MESSAGE))

        serializer = RequestIdSerializer(data=request.data)
        if serializer.is_valid():
            request_id = serializer.validated_data['request_id']
            user_q = get_user_model().objects.filter(request_id=request_id)
            if user_q.exists():
                user = user_q.first()
                # すでにBandMemberが存在したらreturn
                if BandMember.objects.filter(user=user).exists():
                    return Response(error_data(400, constants.MEMBER_ALREADY_EXISTS), status=status.HTTP_400_BAD_REQUEST)
                band_member = BandMember.objects.create(band=band, user=user)
                return Response({'band_member': {'band': band_member.band.id, 'user': band_member.user.id}}, status=status.HTTP_201_CREATED)
        return Response(error_data(400, constants.INVALID_REQUEST_ID), status=status.HTTP_400_BAD_REQUEST)

    return Response({'error': 'Only POST is allowed.'}, status=status.HTTP_400_BAD_REQUEST)


class LeaveBandView(generics.RetrieveDestroyAPIView):
    """
    BandMember脱退(削除)。
    api/bands/members/leave_my_band/
    削除：DELETEする。
    """
    queryset = BandMember.objects.all()
    serializer_class = MemberRequestSerializer
    permission_classes = [IsAuthenticated, IsNotLeaderButMember, IsOwner]

    def get_queryset(self):
        user = self.request.user
        return BandMember.objects.filter(user=user, is_active=True)

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset)
        self.check_object_permissions(self.request, obj)
        return obj


class MyRequestEditView(generics.RetrieveUpdateDestroyAPIView):
    """
    BandMember承認、拒否(削除)。
    api/bands/members/my_request/
    承認：PATCHでis_activeをTrueにする。
    削除：DELETEする。
    """
    queryset = BandMember.objects.all()
    serializer_class = MemberRequestSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self):
        user = self.request.user
        return BandMember.objects.filter(user=user, is_active=False)

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset)
        self.check_object_permissions(self.request, obj)
        return obj


# ==============================
# LiveInfo Views
# ==============================
class LiveInfoListCreateView(generics.ListCreateAPIView):
    """
    Ver2でFilterを追加する。
    """
    queryset = LiveInfo.objects.all()
    serializer_class = LiveInfoSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsLeaderOrReadOnly]

    def perform_create(self, serializer):
        """
        自動的にleaderのBandを紐付け。
        """
        band = Band.objects.filter(leader=self.request.user).first()
        serializer.save(band=band)


class LiveInfoDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = LiveInfo.objects.all()
    serializer_class = LiveInfoSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsLeaderOrReadOnly]


class BandLiveInfoRetrieveView(generics.RetrieveAPIView):
    queryset = Band.objects.all()
    serializer_class = BandLiveInfoSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        ネストされたページネーションはしない。
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data['live_info']
        return Response(data)


# ==============================
# CommentOnLiveInfo Views
# ==============================
class LiveInfoCommentOnLiveInfoView(generics.ListCreateAPIView):
    queryset = CommentOnLiveInfo.objects.all()
    serializer_class = CommentOnLiveInfoSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        """
        kwargsから{pk: 1}を取得
        """
        kwargs = self.kwargs
        live_info = None
        live_info_id = kwargs.get('pk', None)
        try:
            live_info = LiveInfo.objects.get(pk=live_info_id)
        except LiveInfo.DoesNotExist:
            raise NotFound(detail='このバンドは存在しません。')
        return live_info.comments.all()

    def perform_create(self, serializer):
        kwargs = self.kwargs
        live_info = None
        live_info_id = kwargs.get('pk', None)
        try:
            live_info = LiveInfo.objects.get(pk=live_info_id)
        except LiveInfo.DoesNotExist:
            raise NotFound(detail='このライブ情報は存在しません。')
        serializer.save(user=self.request.user, live_info=live_info)


class CommentOnLiveInfoDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = CommentOnLiveInfo.objects.all()
    serializer_class = CommentOnLiveInfoSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticated])
def toggle_band_fans(request, pk):
    """
    GET: Bandをすでにファンになるしているかどうかをis_fan: Boolの形で返す。
    POST: band=pkかつuser=request.userのBandFansを条件に合わせてcreateまたはdeleteする。
    """
    try:
        band = Band.objects.get(pk=pk)
    except Band.DoesNotExist:
        raise NotFound(detail='このバンドは存在しません。')

    bands_member = band.members.all()
    if request.user.as_band_member in bands_member:
        return Response({'detail': '自分のバンドのファンになることはできません。'})

    if request.method == 'GET':
        if BandFans.objects.filter(band=band, user=request.user).exists():
            return Response({'is_fan': True}, status=status.HTTP_200_OK)
        else:
            return Response({'is_fan': False}, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        user = request.user
        count = BandFans.objects.filter(user=user, band=band).count()
        if count == 0:
            band_fans = BandFans.objects.create(user=user, band=band)
            serializer = BandFansSerializer(band_fans)
            # band_fans_countを１足す
            band_fans_count = BandFansCount.objects.filter(band=band).first()
            band_fans_count.count = F('count') + 1
            band_fans_count.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            band_fans = BandFans.objects.filter(user=user, band=band).delete()
            # band_fans_countを１引く
            band_fans_count = BandFansCount.objects.filter(band=band).first()
            band_fans_count.count = F('count') - 1
            band_fans_count.save()
            return Response(status=status.HTTP_204_NO_CONTENT)


class BandFansView(generics.RetrieveAPIView):
    queryset = Band.objects.all()
    serializer_class = PaginatedBandFansSerializer

    def retrieve(self, request, *args, **kwargs):
        """
        ネストされたページネーションはしない。
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data['fans']
        return Response(data)


# ==================================
# 開発用View
# ==================================
class BandMemberRetrieveDestroyView(generics.RetrieveDestroyAPIView):
    """
    BandMember強制削除。
    """
    queryset = BandMember.objects.all()
    serializer_class = BandMemberSerializer
    permission_classes = [IsSuperuser]
