from django.apps import AppConfig


class BandApiConfig(AppConfig):
    name = 'band_api'

    def ready(self):
        from django.db.models.signals import pre_delete, post_save
        from .signals import pre_delete_set_band_id, post_save_set_band_id
        from .models import BandMember
        pre_delete.connect(pre_delete_set_band_id, sender=BandMember)
        post_save.connect(post_save_set_band_id, sender=BandMember)
