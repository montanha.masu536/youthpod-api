from user_api.models import Profile


def pre_delete_set_band_id(sender, instance, **kwargs):
    """
    バンドメンバーが削除（脱退）するときにProfileのband_idをnullにする
    """
    profile = Profile.objects.filter(user=instance.user).first()
    if profile is None:
        print('ERROR: Signal failed... pre_delete_set_band_id')
        return

    profile.band_id = None
    profile.save()


def post_save_set_band_id(sender, instance, created, **kwargs):
    """
    バンドメンバーが承認するときにProfileのband_idをTrueにする。
    """
    profile = Profile.objects.filter(user=instance.user).first()
    if profile is None:
        print('ERROR: Signal failed... post_save_set_band_id')
        return

    if not created:
        if instance.is_active:
            profile.band_id = instance.band.id
            profile.save()
