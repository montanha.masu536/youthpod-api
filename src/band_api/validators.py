from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import re


def validate_maps_url(url):
    """
    AppleおよびGoogle MapのみをURLとして許可する。
    """
    regex = re.compile(r'^https://maps\.apple\.com/.+|^https://goo\.gl/maps/.+', re.DOTALL)
    match = re.match(regex, url)
    if match is None:
        raise ValidationError(
            _('Apple MapsまたはGoogle MapsのURLを入力してください。'),
            params={'url': url},
        )
