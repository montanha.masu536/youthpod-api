from django.urls import path, include
from .views import BandListCreateView, BandRetrieveView, BandMemberListCreateView, create_band_member_with_request_id, BandMemberRetrieveDestroyView, BandMemberSetIsLeaderView, BandRetrieveDestroyView, MyRequestEditView, LeaveBandView, LiveInfoListCreateView, LiveInfoDetailView, BandLiveInfoRetrieveView, LiveInfoCommentOnLiveInfoView, CommentOnLiveInfoDetailView, toggle_band_fans, BandFansView, MyBandDetailView
from music_api.views import BandMusicsRetrieveView


bands_urlpatterns = [
    # Band
    path('', BandListCreateView.as_view(), name='band_list_create'),
    path('<str:pk>/', BandRetrieveView.as_view(), name='band_detail'),
    path('<str:pk>/actions/delete/', BandRetrieveDestroyView.as_view(), name='band_delete'),
    path('<str:pk>/actions/favorite/', toggle_band_fans, name='band_favorite'),
    path('<str:pk>/live_info/', BandLiveInfoRetrieveView.as_view(), name='band_detail_live_info'),
    path('<str:pk>/musics/', BandMusicsRetrieveView.as_view(), name='band_detail_musics'),
    path('<str:pk>/fans/', BandFansView.as_view(), name='band_detail_fans'),
    path('my/band/', MyBandDetailView.as_view(), name='my_band'),
]

members_urlpatterns = [
    # BandMembers
    path('', BandMemberListCreateView.as_view(), name='band_members_list'),
    path('<int:pk>/set_leader/', BandMemberSetIsLeaderView.as_view(), name='member_set_leader'),
    path('actions/send_request/', create_band_member_with_request_id, name='send_member_request'),
    path('<int:pk>/actions/delete/', BandMemberRetrieveDestroyView.as_view(), name='band_member_delete'),
    path('my/request/', MyRequestEditView.as_view(), name='my_request'),
    path('leave_my_band/', LeaveBandView.as_view(), name='leave_my_band'),
]

live_info_urlpatterns = [
    # LiveInfo
    path('', LiveInfoListCreateView.as_view(), name='live_info'),
    path('<int:pk>/', LiveInfoDetailView.as_view(), name='live_info_detail'),
    # CommentOnLiveInfo
    path('comments/<int:pk>/', CommentOnLiveInfoDetailView.as_view(), name='comment_on_live_info_detail'),
    path('<int:pk>/comments/', LiveInfoCommentOnLiveInfoView.as_view(), name='live_info_comment_on_live_info_list_create'),
]

urlpatterns = [
    path('bands/', include(bands_urlpatterns)),
    path('members/', include(members_urlpatterns)),
    path('live_info/', include(live_info_urlpatterns)),
]
