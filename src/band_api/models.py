from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import validate_image_file_extension
from user_api.models import Profile
from .validators import validate_maps_url
from youthpod_api.validators import validate_image_size


def band_img_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    return f'band_api/band_images/band_{instance.id}.{ext}'


def live_info_img_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    return f'band_api/live_info_images/band_{instance.band.id}_liveinfo_{instance.id}.{ext}'


class BandManager(models.Manager):
    """
    Band作成時に、leaderのBandMember、それに紐づくBandFansCountオブジェクトを作成する。
    """
    def create(self, *args, **kwargs):
        band = super(BandManager, self).create(*args, **kwargs)
        member = BandMember.objects.create(band=band, user=band.leader, is_active=True, is_leader=True)
        member.save()
        band_fans_count = BandFansCount(band=band, count=0)
        band_fans_count.save()
        return band


class Band(models.Model):
    """
    バンドの実体とプロフィールを合わせたモデル。
    regionはchoiceにせず自由度を高く保つ。
    hiatus_dateは活動休止日。
    """
    id = models.CharField(primary_key=True, unique=True, max_length=50)
    bandname = models.CharField(max_length=30)
    img = models.ImageField(upload_to=band_img_upload_to, null=True, blank=True, max_length=255, validators=[validate_image_file_extension, validate_image_size])
    region = models.CharField(max_length=15, null=True, blank=True)
    description = models.TextField(max_length=3000, null=True, blank=True)
    leader = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='band')
    is_members_list_hidden = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    established_on = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    hiatus_date = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    fans = models.ManyToManyField(get_user_model(), through='BandFans', related_name="favorite_bands")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = BandManager()

    class Meta:
        ordering = ['updated_at']

    def __str__(self):
        return self.id


class BandMemberManager(models.Manager):
    def create(self, *args, **kwargs):
        member = super(BandMemberManager, self).create(*args, **kwargs)
        if member.is_active:
            profile = Profile.objects.filter(user=member.user).first()
            profile.band_id = member.band.id
            profile.save()
        return member


class BandMember(models.Model):
    """
    BandとUserを繋ぐテーブル。
    """
    band = models.ForeignKey(Band, on_delete=models.CASCADE, related_name='members')
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name='as_band_member')
    is_active = models.BooleanField(default=False)
    is_leader = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = BandMemberManager()

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return self.user.profile.username


class LiveInfo(models.Model):
    """
    ライブ情報のモデル。
    start_atは未定可。
    map_urlはApple or Google mapのみ可。
    """
    band = models.ForeignKey(Band, on_delete=models.CASCADE, related_name='live_info')
    img = models.ImageField(upload_to=live_info_img_upload_to, max_length=255, null=True, blank=True, validators=[validate_image_file_extension, validate_image_size])
    description = models.TextField(max_length=1500)
    venue_prefecture = models.CharField(max_length=10, null=True, blank=True)
    maps_url = models.URLField(max_length=500, null=True, blank=True, validators=[validate_maps_url])
    start_at = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['updated_at']

    def __str__(self):
        return self.description[:10]


class CommentOnLiveInfo(models.Model):
    """
    ライブ情報につくコメント
    """
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='commnets_on_live_info')
    live_info = models.ForeignKey(LiveInfo, on_delete=models.CASCADE, related_name='comments')
    comment = models.TextField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']


class BandFans(models.Model):
    """
    ファンになったバンドを表す中間テーブル
    """
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    band = models.ForeignKey(Band, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return self.user


class BandFansCount(models.Model):
    """
    BandFansオブジェクトが、
    createで+1、
    deleteで-1するように上書きする。
    """
    band = models.OneToOneField(Band, related_name='fans_count', on_delete=models.CASCADE, primary_key=True)
    count = models.IntegerField(default=0)

    def __str__(self):
        return str(self.count)
