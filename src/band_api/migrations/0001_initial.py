# Generated by Django 3.2.2 on 2021-05-15 07:29

import band_api.models
import band_api.validators
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import youthpod_api.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Band',
            fields=[
                ('id', models.CharField(max_length=50, primary_key=True, serialize=False, unique=True)),
                ('bandname', models.CharField(max_length=30)),
                ('img', models.ImageField(blank=True, max_length=255, null=True, upload_to=band_api.models.band_img_upload_to, validators=[django.core.validators.validate_image_file_extension, youthpod_api.validators.validate_image_size])),
                ('region', models.CharField(blank=True, max_length=15, null=True)),
                ('description', models.TextField(blank=True, max_length=3000, null=True)),
                ('is_members_list_hidden', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('established_on', models.DateField(blank=True, null=True)),
                ('hiatus_date', models.DateField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['updated_at'],
            },
        ),
        migrations.CreateModel(
            name='BandFans',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
        migrations.CreateModel(
            name='BandMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_active', models.BooleanField(default=False)),
                ('is_leader', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
        migrations.CreateModel(
            name='BandFansCount',
            fields=[
                ('band', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='fans_count', serialize=False, to='band_api.band')),
                ('count', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='LiveInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(blank=True, max_length=255, null=True, upload_to=band_api.models.live_info_img_upload_to, validators=[django.core.validators.validate_image_file_extension, youthpod_api.validators.validate_image_size])),
                ('description', models.TextField(max_length=1500)),
                ('venue_prefecture', models.CharField(blank=True, max_length=10, null=True)),
                ('maps_url', models.URLField(blank=True, max_length=500, null=True, validators=[band_api.validators.validate_maps_url])),
                ('start_at', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('band', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='live_info', to='band_api.band')),
            ],
            options={
                'ordering': ['updated_at'],
            },
        ),
        migrations.CreateModel(
            name='CommentOnLiveInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.TextField(max_length=1000)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('live_info', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='band_api.liveinfo')),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
    ]
