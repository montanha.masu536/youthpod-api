from rest_framework import serializers
from user_api.serializers import ProfileSerializer, UserProfileSerializer
from .models import Band, BandMember, LiveInfo, CommentOnLiveInfo, BandFans, BandFansCount
from youthpod_api.paginators import RelationPaginator
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist


# ==============================
# Plain Serializers
# ==============================
class PlainBandMemberSerializer(serializers.ModelSerializer):
    """
    簡易版BandMemberSerializer。用途としては、リスト表示に必要な最低限の情報が欲しい時に使う。
    """
    username = serializers.CharField(read_only=True, source='user.profile.username')
    img = serializers.ImageField(read_only=True, source='user.profile.img')
    profile = serializers.PrimaryKeyRelatedField(read_only=True, source='user.profile')

    class Meta:
        model = BandMember
        fields = ('id', 'username', 'img', 'is_leader', 'profile')
        read_only_fields = ('id', 'username', 'img', 'is_leader', 'profile')


class PlainLiveInfoSerializer(serializers.ModelSerializer):
    """
    簡易版LiveInfoSerializer。
    """
    url = serializers.HyperlinkedIdentityField(view_name='live_info_detail')

    class Meta:
        model = LiveInfo
        fields = ('url', 'id', 'venue_prefecture', 'start_at', 'updated_at')
        read_only_fields = ('url', 'id', 'venue_prefecture', 'start_at', 'updated_at')


class PlainBandSerializer(serializers.ModelSerializer):
    """
    簡易版BandSerializer。
    """
    class Meta:
        model = Band
        fields = ('id', 'bandname', 'img')


# ==============================
# BandMember
# ==============================
class BandMemberSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")
    profile = serializers.HyperlinkedRelatedField(read_only=True, source='user.profile', view_name='profile_detail')

    class Meta:
        model = BandMember
        fields = ('id', 'band', 'user', 'is_active', 'is_leader', 'created_at', 'profile')
        read_only_fields = ('id', 'band', 'user', 'is_active', 'created_at', 'profile')

    def update(self, instance, validated_data):
        band = instance.band
        band.leader = instance.user
        band.save()
        return super(BandMemberSerializer, self).update(instance, validated_data)


class MemberRequestSerializer(serializers.ModelSerializer):
    bandname = serializers.CharField(read_only=True, source='band.bandname')
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = BandMember
        fields = ('id', 'band', 'bandname', 'is_active', 'created_at')
        read_only_fields = ('id', 'band', 'bandname', 'created_at')


class RequestIdSerializer(serializers.Serializer):
    """
    request_idをシリアライズするために用いる。
    """
    request_id = serializers.CharField(max_length=16, min_length=8, required=True)


# ==============================
# LiveInfo
# ==============================
class LiveInfoSerializer(serializers.ModelSerializer):
    band = PlainBandSerializer(read_only=True)
    start_at = serializers.DateTimeField(allow_null=True, format="%Y-%m-%d %H:%M", required=False)
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")
    updated_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = LiveInfo
        fields = ('id', 'band', 'img', 'description', 'venue_prefecture', 'maps_url', 'start_at', 'created_at', 'updated_at')
        read_only_fields = ('id', 'band', 'created_at', 'updated_at')
        extra_kwargs = {
            'img': {'required': False},
            'maps_url': {'required': False}
        }


class BandLiveInfoSerializer(serializers.ModelSerializer):
    live_info = serializers.SerializerMethodField()

    class Meta:
        model = Band
        fields = ('live_info',)
        read_only_fields = ('live_info',)

    def get_live_info(self, obj):
        queryset = obj.live_info.all()
        request = self.context.get('request')
        serializer = LiveInfoSerializer(queryset, many=True, context={'request': request})
        paginator = RelationPaginator()
        paginated_data = paginator.paginate_queryset(queryset=serializer.data, request=request)
        result = paginator.get_paginated_response(paginated_data)
        return result


# ==============================
# Band
# ==============================
class BandSerializer(serializers.ModelSerializer):
    """
    @params is_leader: request userがこのバンドのleaderならtrue
    """
    established_on = serializers.DateField(allow_null=True, format="%Y-%m-%d", required=False)
    hiatus_date = serializers.DateField(allow_null=True, format="%Y-%m-%d", required=False)
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")
    updated_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")
    members = PlainBandMemberSerializer(read_only=True, many=True)
    num_of_musics = serializers.SerializerMethodField()
    fans_count = serializers.IntegerField(read_only=True, source='fans_count.count')
    is_fan = serializers.SerializerMethodField()
    is_my_band = serializers.SerializerMethodField()
    is_leader = serializers.SerializerMethodField()
    total_likes = serializers.SerializerMethodField()

    class Meta:
        model = Band
        fields = ('id', 'bandname', 'img', 'region', 'description', 'leader', 'is_members_list_hidden', 'is_active', 'established_on', 'hiatus_date', 'created_at', 'updated_at', 'members', 'num_of_musics', 'fans_count', 'is_fan', 'is_my_band', 'is_leader', 'total_likes')
        read_only_fields = ('leader', 'is_active', 'fans_count', 'is_fan', 'is_my_band', 'is_leader', 'total_likes')

    def get_num_of_musics(self, obj):
        count = obj.musics.count()
        return count

    def get_is_fan(self, obj):
        """
        フロントエンド用にすでにファンかどうかをBoolで返す
        """
        request = self.context.get('request')
        result = False
        # =============================================
        # Check: is user Authenticated?
        # =============================================
        if isinstance(request.user, get_user_model()):
            favorite_bands = request.user.favorite_bands.all()
            if obj in favorite_bands:
                result = True
        return result

    def get_is_my_band(self, obj):
        """
        フロントエンド用に自分のバンドかどうかをBoolで返す
        """
        request = self.context.get('request')
        user = request.user
        result = False
        # =============================================
        # Check: is user Authenticated?
        # =============================================
        if isinstance(user, get_user_model()):
            try:
                band = user.as_band_member.band
            except ObjectDoesNotExist:
                return result
            result = obj == band
        return result

    def get_is_leader(self, obj):
        request = self.context.get('request')
        member = None
        # =============================================
        # Check 1: is user Authenticated?
        # =============================================
        if isinstance(request.user, get_user_model()):
            member = BandMember.objects.filter(user=request.user).first()
        # =============================================
        # Check 2: does user has member object?
        # =============================================
        if isinstance(member, BandMember):
            if member.band == obj:
                return member.is_leader
        return False

    def get_total_likes(self, obj):
        musics = obj.musics.all()
        total = 0
        for music in musics:
            total += music.like_count.count
        return total


class MyBandSerializer(BandSerializer):
    class Meta(BandSerializer.Meta):
        extra_kwargs = {'id': {'read_only': True}}


class PaginatedBandFansSerializer(serializers.ModelSerializer):
    """
    Bandのファンを全てページネーションで取得する
    """
    fans = serializers.SerializerMethodField()

    class Meta:
        model = Band
        fields = ('fans',)
        read_only_fields = ('fans',)

    def get_fans(self, obj):
        queryset = obj.fans.all()
        request = self.context.get('request')
        serializer = UserProfileSerializer(queryset, many=True, context={'request': request})
        paginator = RelationPaginator()
        paginated_data = paginator.paginate_queryset(queryset=serializer.data, request=request)
        result = paginator.get_paginated_response(paginated_data)
        return result


# ==============================
# CommentOnLiveInfo
# ==============================
class CommentOnLiveInfoSerializer(serializers.ModelSerializer):
    user_profile = ProfileSerializer(source='user.profile', read_only=True)
    created_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")
    updated_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = CommentOnLiveInfo
        fields = ('id', 'user', 'comment', 'created_at', 'updated_at')
        read_only_fields = ('id', 'user', 'created_at', 'updated_at')


# ==============================
# BandFans
# ==============================
class BandFansSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = BandFans
        fields = ('user', 'band', 'created')


class BandFansCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BandFansCount
        fields = ('band', 'count')


class UserFavoriteBandsSerializer(serializers.ModelSerializer):
    """
    Userのliked_musicsをread_onlyで取得する
    """
    favorite_bands = serializers.SerializerMethodField()

    class Meta:
        model = get_user_model()
        fields = ('favorite_bands',)
        read_only_fields = ('favorite_bands',)

    def get_favorite_bands(self, obj):
        queryset = obj.favorite_bands.all()
        request = self.context.get('request')
        serializer = BandSerializer(queryset, many=True, context={'request': request})
        paginator = RelationPaginator()
        paginated_data = paginator.paginate_queryset(queryset=serializer.data, request=request)
        result = paginator.get_paginated_response(paginated_data)
        return result
