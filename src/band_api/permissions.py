from rest_framework import permissions
from .models import BandMember
from user_api.decorators import allow_superuser


class DoesNotHaveAnyBandOrReadOnly(permissions.BasePermission):
    """
    User cannot belong to more than one band.
    """
    message = 'すでにバンドに所属しています。by YouthPod team.'

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        # Requested user must not have BandMember model.
        else:
            return not BandMember.objects.filter(user=request.user).exists()


class IsLeaderOrReadOnly(permissions.BasePermission):
    """
    Only the leader can invite other members to the band.
    """
    message = 'リーダーではありません。'

    @allow_superuser
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        # Requested user must not have BandMember model.
        else:
            return BandMember.objects.filter(is_leader=True, user=request.user).exists()


class IsLeader(permissions.BasePermission):
    """
    Check if the requested user is a leader or not.
    """
    message = 'リーダーではありません。'

    @allow_superuser
    def has_permission(self, request, view):
        return BandMember.objects.filter(is_leader=True, user=request.user).exists()


class IsNotLeader(permissions.BasePermission):
    """
    Check if the requested user is a leader or not.
    """
    message = 'リーダーです。'

    @allow_superuser
    def has_permission(self, request, view):
        return not BandMember.objects.filter(is_leader=True, user=request.user).exists()


class IsNotLeaderButMember(permissions.BasePermission):
    """
    Check if the requested user is a leader or not.
    """
    message = 'リーダーです。'

    @allow_superuser
    def has_permission(self, request, view):
        flag1 = not BandMember.objects.filter(is_leader=True, user=request.user).exists()
        flag2 = request.user.profile.band_id is not None
        return flag1 and flag2


class IsTheBandsLeader(permissions.BasePermission):
    """
    Bandはleaderのみが削除可能。
    """
    @allow_superuser
    def has_object_permission(self, request, view, obj):
        return obj.leader == request.user


class IsTheBandsLeaderOrReadOnly(permissions.BasePermission):
    """
    Bandはleaderのみが編集可能。
    """

    @allow_superuser
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.leader == request.user
