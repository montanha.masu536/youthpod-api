from django.test import TransactionTestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient
from rest_framework import status
from user_api.models import Profile
from band_api.models import Band


class AuthorizedUserApiTests(TransactionTestCase):
    """
    状況:
    認証済みユーザーがleaderとしてBandに所属
    """
    reset_sequences = True

    def setUp(self):
        # urls
        self.url_band_list_create = reverse('band_list_create')
        # dummies
        self.user = get_user_model().objects.create_user(email='montanha.masu536@gmail.com', password='Awesome123TTT')
        self.user_profile = Profile.objects.create(user=self.user, username='dummy')
        self.band = Band.objects.create(id="dummy_band", bandname="Dummy", leader=self.user)
        # others
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    def test_2_1_should_get_all_bands(self):
        res = self.client.get(self.url_band_list_create)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(res.data["results"][0]["id"], self.band.id)
        self.assertEqual(res.data["results"][0]["leader"], self.user.id)
