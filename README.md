# 環境
```
Frontend: 別リポジトリ
Swift 5
iOS 13

Backend: このリポジトリ
Python == 3.8
Django == 3.2.2
Django REST Framework == 3.12.4
```

# YouthPod
```
学生バンドを盛り上げるための音楽配信アプリです。
出版されている一般的な音楽を聞くように、毎日の中で学生バンドの
曲が聞けるようになったらいいなと言う思いで作っています。
主に楽曲のアップロード・取得、活用することでバンド活動をさらに
活発にできるような機能を盛り込んでいます。
```


# Author
* Taichi Masuyama
* YouthPod team.
* montanha.masu536@gmail.com

# License
"YouthPod" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License)
