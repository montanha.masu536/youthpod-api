FROM python:3.8.9-slim-buster
LABEL maintainer="montanha.masu536@gmail.com"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONBUFFERED 1
ENV PATH="/scripts:${PATH}"

# Create a group and user to run our app
# --no-log-init ユーザーをlastlogとfaillogのデータベースに追加しない
# -r システムアカウントを作成する(ホームディレクトリを作成しない)
# -g set primary group
ARG APP_USER=appuser
RUN groupadd -r ${APP_USER} && useradd --no-log-init -r -g ${APP_USER} ${APP_USER}

# Install packages needed to run your application (not build deps):
#   mime-support -- for mime types when serving static files
#   postgresql-client -- for running database commands
# You need to recreate the /usr/share/man/man{1..8} directories first because
# they were clobbered by a parent image.
# You need to do this before installing any software because /man/* directries
# are there for storing some documents of softwares that may be installed.
RUN set -ex \
    && RUN_DEPS=" \
    libpcre3 \
    mime-support \
    postgresql-client \
    " \
    && seq 1 8 | xargs -I{} mkdir -p /usr/share/man/man{} \
    && apt-get update && apt-get install -y --no-install-recommends $RUN_DEPS \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip
COPY ./requirements.txt /requirements.txt

# Install build deps, then run `pip install`, then remove unneeded build deps all in a single step.
RUN set -ex \
    && BUILD_DEPS=" \
    build-essential \
    libpcre3-dev \
    libpq-dev \
    " \
    && apt-get update && apt-get install -y --no-install-recommends $BUILD_DEPS \
    && pip install --no-cache-dir -r /requirements.txt \
    \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false $BUILD_DEPS \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app
COPY ./src /app
COPY ./scripts /scripts

RUN chmod +x /scripts/*

# folder to serve static files by nginx
RUN mkdir -p /vol/web/static
RUN mkdir -p /vol/web/media


# add full access to owner and read-access to everyone else
RUN chown -R ${APP_USER}:${APP_USER} /vol
RUN chmod -R 755 /vol/web
RUN chown -R ${APP_USER}:${APP_USER} /app
RUN chmod -R 755 /app

# switch user from root user
USER ${APP_USER}:${APP_USER}

VOLUME /vol/web

CMD ["entrypoint.sh"]
